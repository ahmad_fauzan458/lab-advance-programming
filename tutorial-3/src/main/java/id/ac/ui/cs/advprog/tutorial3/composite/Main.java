package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Cto;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.BackendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.FrontendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.NetworkExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.SecurityExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.UiUxDesigner;

public class Main {
    private static Company company;
    private static Ceo luffy;
    private static Cto zorro;
    private static BackendProgrammer franky;
    private static BackendProgrammer usopp;
    private static FrontendProgrammer nami;
    private static FrontendProgrammer robin;
    private static UiUxDesigner sanji;
    private static NetworkExpert brook;
    private static SecurityExpert chopper;

    public static void main(String[] args) {
        company = new Company();

        luffy = new Ceo("Luffy", 500000.00);
        company.addEmployee(luffy);

        zorro = new Cto("Zorro", 320000.00);
        company.addEmployee(zorro);

        franky = new BackendProgrammer("Franky", 94000.00);
        company.addEmployee(franky);

        usopp = new BackendProgrammer("Usopp", 200000.00);
        company.addEmployee(usopp);

        nami = new FrontendProgrammer("Nami", 66000.00);
        company.addEmployee(nami);

        robin = new FrontendProgrammer("Robin", 130000.00);
        company.addEmployee(robin);

        sanji = new UiUxDesigner("sanji", 177000.00);
        company.addEmployee(sanji);

        brook = new NetworkExpert("Brook", 83000.00);
        company.addEmployee(brook);

        chopper = new SecurityExpert("Chopper", 70000.00);
        company.addEmployee(chopper);

        System.out.println("Employees :");
        company.getAllEmployees().stream().forEach(e -> System.out.println("Name : " + e.getName()
                + ", Role : " + e.getRole() + ", Salary : " + e.getSalary()));
        System.out.println("Net Salary :" + company.getNetSalaries());

    }
}
