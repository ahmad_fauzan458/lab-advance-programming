package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class UiUxDesigner extends Employees {

    public UiUxDesigner(String name, double salary) throws IllegalArgumentException {
        this.role = "UI/UX Designer";
        this.name = name;
        if (salary < 90000.00) {
            throw new IllegalArgumentException();
        } else {
            this.salary = salary;
        }
    }

    public double getSalary() {
        return this.salary;
    }
}
