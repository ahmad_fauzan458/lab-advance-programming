package id.ac.ui.cs.advprog.tutorial3.composite.higherups;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class Cto extends Employees {

    public Cto(String name, double salary) throws IllegalArgumentException {
        this.role = "CTO";
        this.name = name;
        if (salary < 100000.00) {
            throw new IllegalArgumentException();
        } else {
            this.salary = salary;
        }
    }

    public double getSalary() {
        return this.salary;
    }
}
