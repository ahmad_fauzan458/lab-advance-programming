package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class SecurityExpert extends Employees {

    public SecurityExpert(String name, double salary) throws IllegalArgumentException {
        this.role = "Security Expert";
        this.name = name;
        if (salary < 70000.00) {
            throw new IllegalArgumentException();
        } else {
            this.salary = salary;
        }
    }

    public double getSalary() {
        return this.salary;
    }

}
