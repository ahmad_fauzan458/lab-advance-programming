package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class FrontendProgrammer extends Employees {

    public FrontendProgrammer(String name, double salary) throws IllegalArgumentException {
        this.role = "Front End Programmer";
        this.name = name;
        if (salary < 30000.00) {
            throw new IllegalArgumentException();
        } else {
            this.salary = salary;
        }
    }

    public double getSalary() {
        return this.salary;
    }
}
