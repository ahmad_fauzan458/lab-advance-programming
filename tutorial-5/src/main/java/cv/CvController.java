package cv;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class CvController {

    @GetMapping("/cv")
    public String cv(@RequestParam(name = "visitor", required = false)
                             String visitor, Model model) {
        String content;
        if (visitor == null || visitor.isEmpty()) {
            content = "This is my CV";
        } else {
            content = visitor + ", I hope you interested to hire me";
        }
        model.addAttribute("content", content);
        return "cv";
    }
}
