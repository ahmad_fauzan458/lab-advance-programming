var theme1 = { 
	body_color:"rgb(218, 229, 255)",
	navbar_color:"rgb(0, 38, 79)", 
	panel_heading_color:"rgb(63, 137, 224)",
	button_class: "btn-primary",
	hover_nav_side: "side-navbar-theme1",
	hover_nav_top: "top-navbar-theme1"};

var theme2 = {
	body_color:"rgb(177, 227, 227)",
	navbar_color:"rgb(2, 54, 24)", 
	panel_heading_color:"rgb(60, 179, 113)",
	button_class: "btn-success",
	hover_side_nav: "side-navbar-theme2",
	hover_top_nav: "top-navbar-theme2"};

function loadTheme1() {
	$('.custom-body').css('background-color', theme1.body_color);	
	$('.side-navbar').css('background-color', theme1.navbar_color);
	$('.top-navbar').css('background-color', theme1.navbar_color);
	$('.panel-heading-custom').css('background-color', theme1.panel_heading_color);
	$(".header-img-1").show()
	$(".header-img-2").hide()
	$(".nav-button").removeClass(theme2.button_class).addClass(theme1.button_class);
	$(".status-button").removeClass(theme2.button_class).addClass(theme1.button_class);
	$(".side-navbar").removeClass(theme2.hover_side_nav).addClass(theme1.hover_side_nav);
	$(".top-navbar").removeClass(theme2.hover_top_nav).addClass(theme1.hover_top_nav);
}

function loadTheme2() {
	$('.custom-body').css('background-color', theme2.body_color);	
	$('.side-navbar').css('background-color', theme2.navbar_color);
	$('.top-navbar').css('background-color', theme2.navbar_color);
	$('.panel-heading-custom').css('background-color', theme2.panel_heading_color);
	$(".header-img-1").hide()
	$(".header-img-2").show()
	$(".nav-button").removeClass(theme1.button_class).addClass(theme2.button_class);
	$(".status-button").removeClass(theme1.button_class).addClass(theme2.button_class);
	$(".side-navbar").removeClass(theme1.hover_side_nav).addClass(theme2.hover_side_nav);
	$(".top-navbar").removeClass(theme1.hover_top_nav).addClass(theme2.hover_top_nav);
}


function accordion(){

	$('.accordion').click(function() {
	    var panel = this.nextElementSibling;

	    if (panel.style.maxHeight){
	      panel.style.maxHeight = null;
	    } else {
	      panel.style.maxHeight = panel.scrollHeight + "px";
	    } 
	});
}

function loading() {
  var elem = document.getElementById("progress-bar");    
  var width = 1;
  var id = setInterval(frame, 10);
  function frame() {
    if (width >= 100) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
    }
  }
}

function fadeinFadeout() {
    $(".progress-view").fadeOut(2500, function() {
        $(".custom-body-2").fadeIn(1000);        
    });
};


$(document).ready(function(){

	loading();
	fadeinFadeout();

	if (!localStorage.theme) {
		localStorage.theme = "theme1";
	}

	switch(localStorage.theme) {
	    case "theme1":
	        loadTheme1();
	        break;
	    case "theme2":
	        loadTheme2();
	        break;
	}	

	$(".nav-button").click(function(){		
		if (localStorage.theme == "theme1"){
			loadTheme2();
			localStorage.theme = "theme2";
		}
		else {
			loadTheme1();
	    	localStorage.theme = "theme1";
	    }
    });
	accordion();
});




