package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class ThinDough implements Dough {
    public String toString() {
        return "Thin Dough";
    }
}
