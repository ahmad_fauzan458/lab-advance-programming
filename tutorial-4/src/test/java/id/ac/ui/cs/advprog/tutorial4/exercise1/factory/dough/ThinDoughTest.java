package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class ThinDoughTest {
    private ThinDough thinDough;

    @Before
    public void setUp() {
        thinDough = new ThinDough();
    }

    @Test
    public void checkToString() {
        assertEquals(thinDough.toString(), "Thin Dough");
    }
}