package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class FrozenClamsTest {
    private FrozenClams frozenClams;

    @Before
    public void setUp() {
        frozenClams = new FrozenClams();
    }

    @Test
    public void checkToString() {
        assertEquals(frozenClams.toString(), "Frozen Clams from Chesapeake Bay");
    }
}