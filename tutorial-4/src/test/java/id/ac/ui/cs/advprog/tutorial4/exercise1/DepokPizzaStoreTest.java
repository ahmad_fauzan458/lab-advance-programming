package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import org.junit.Before;
import org.junit.Test;

public class DepokPizzaStoreTest {
    private PizzaStore dpkStore;
    private Pizza pizza;

    @Before
    public void setUp() {
        dpkStore = new DepokPizzaStore();
    }

    @Test
    public void checkCheesePizza() {
        pizza = dpkStore.orderPizza("cheese");
        assertEquals(pizza.toString(), "---- Depok Style Cheese Pizza ----\n"
                + "ThickCrust style extra thick crust dough\n"
                + "Andalam sauce\n"
                + "Shredded Manchego\n");
    }

    @Test
    public void checkClamPizza() {
        pizza = dpkStore.orderPizza("clam");
        assertEquals(pizza.toString(), "---- Depok Style Clam Pizza ----\n"
                + "ThickCrust style extra thick crust dough\n"
                + "Andalam sauce\n"
                + "Shredded Manchego\n"
                + "Hot Clams from Montquiero Bay\n");
    }

    @Test
    public void checkVeggiePizza() {
        pizza = dpkStore.orderPizza("veggie");
        assertEquals(pizza.toString(), "---- Depok Style Veggie Pizza ----\n"
                + "ThickCrust style extra thick crust dough\n"
                + "Andalam sauce\n"
                + "Shredded Manchego\n"
                + "Garlic, Onion, Cabbage\n");
    }
}