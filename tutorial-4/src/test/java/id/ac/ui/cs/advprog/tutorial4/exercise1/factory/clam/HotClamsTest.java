package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class HotClamsTest {
    private HotClams hotClams;

    @Before
    public void setUp() {
        hotClams = new HotClams();
    }

    @Test
    public void checkToString() {
        assertEquals(hotClams.toString(), "Hot Clams from Montquiero Bay");
    }
}