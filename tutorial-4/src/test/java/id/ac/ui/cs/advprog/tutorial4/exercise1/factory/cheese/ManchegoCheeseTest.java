package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class ManchegoCheeseTest {
    private ManchegoCheese manchegoCheese;

    @Before
    public void setUp() {
        manchegoCheese = new ManchegoCheese();
    }

    @Test
    public void checkToString() {
        assertEquals(manchegoCheese.toString(), "Shredded Manchego");
    }
}
