package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class MozzarellaCheeseTest {
    private MozzarellaCheese mozarellaCheese;

    @Before
    public void setUp() {
        mozarellaCheese = new MozzarellaCheese();
    }

    @Test
    public void checkToString() {
        assertEquals(mozarellaCheese.toString(), "Shredded Mozzarella");
    }
}
