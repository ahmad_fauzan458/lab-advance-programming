package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class AndalamSauceTest {
    private AndalamSauce andalamSauce;

    @Before
    public void setUp() {
        andalamSauce = new AndalamSauce();
    }

    @Test
    public void checkToString() {
        assertEquals(andalamSauce.toString(), "Andalam sauce");
    }
}